package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

    @Resource
    BrandService brandService;

    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        /*brandEntity.setDescript("");
        brandEntity.setName("魅族");
        boolean save = brandService.save(brandEntity);
        System.out.println("保存成功");*/
        brandEntity.setBrandId(1l);
        brandEntity.setName("华为荣耀");
        brandService.updateById(brandEntity);
    }

    @Test
    void queryTest(){
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<BrandEntity>();
        wrapper.ge("brand_id", 2L);
        List<BrandEntity> list = brandService.list(wrapper);
        list.forEach(b-> System.out.println(b));
    }

}
